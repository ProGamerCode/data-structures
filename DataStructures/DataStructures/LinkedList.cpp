#include "stdafx.h"
#include <iostream>
#include "LinkedList.h"


LinkedList::LinkedList()
{
	head = NULL;
	count = 0;
}

LinkedList::~LinkedList()
{
	// clear out the linked list
	LLNode * tmp = head;
	while (head != NULL) {
		tmp = head;
		head = head->next;
		delete tmp;
	}
}

void LinkedList::AddNode(int val)
{
	LLNode * newNode = new LLNode(val);
	newNode->next = head;
	head = newNode;
	count++;
}

void LinkedList::AddNodeToEnd(int val)
{
	LLNode * current = head;
	LLNode * newNode = new LLNode(val);

	// If empty, then simply assign it
	if (IsEmpty()) head = newNode;
	else if (count == 1) head->next = newNode; // if only one node, then point it to the newNode

	// Traverse to the last node
	while (current->next != NULL) {
		current = current->next;
	}

	current->next = newNode;
}

bool LinkedList::AddNodeAfterValue(int newValue, int locationValue)
{
	bool success = false;
	LLNode * current = head; // Use the current pointer to traverse the list
	if (IsEmpty()) return false;
	while (current != NULL) {
		if (current->value == locationValue)
		{
			LLNode * newNode = new LLNode(newValue);
			newNode->next = current->next;
			current->next = newNode;
			success = true;
			break;
		}
		current = current->next;
	}
	return success;
}

LLNode * LinkedList::GetHeadNode()
{
	return head;
}

bool LinkedList::RemoveFirst()
{
	bool success = false;
	if (head != NULL) {
		LLNode * tmp = head;
		head = head->next;
		delete tmp;
		success = true;
	}
	return success;
}

bool LinkedList::RemoveNode(int val)
{
	bool success = false;
	LLNode * current;
	LLNode * runner;
	runner = current = head;

	// If linkedlist is empty, then do nothing
	if (IsEmpty()) return false;

	// if first node is it
	if (runner->value == val) {
		LLNode * tmp = runner;
		head = runner->next;
		runner = current = head;
		delete tmp;
		success = true;
	}
	else { // traverse remainder of list
		runner = runner->next;
		while (runner != NULL) {
			if (runner->value == val)
			{	// we've found it
				LLNode * tmp = runner;
				current->next = runner->next;
				delete tmp;
				success = true;
				break;
			}
			runner = runner->next;
			current = current->next;
		}
	}
	return success;
}

void LinkedList::PrintNodes()
{
	LLNode * current = head;
	int i = 1;
	while (current != NULL) {
		// Print data
		std::cout << i++ << ": " << current->value << std::endl;
		// Move current pointer
		current = current->next;
	}
}

bool LinkedList::IsEmpty()
{
	return (count == 0 ? true : false);
}
