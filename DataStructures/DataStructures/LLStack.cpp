#include "stdafx.h"
#include "LLStack.h"


LLStack::LLStack()
{
	count = 0;
	LL = new LinkedList();
}


LLStack::~LLStack()
{
	delete LL;
}

void LLStack::Push(int value)
{
	count++;
	LL->AddNode(value);
}

void LLStack::Pop()
{
	LLNode * tmp = LL->GetHeadNode();
	if (tmp != NULL) {
		LL->RemoveFirst();
		count--;
	}
}

int LLStack::Peek()
{
	LLNode * tmp = LL->GetHeadNode();
	if (tmp != NULL) {
		return tmp->value;
	}
	return -99;
}

int LLStack::GetLength()
{
	return count;
}

void LLStack::PrintAll()
{
	LL->PrintNodes();
}
