#pragma once
#include "LLNode.h"

class LinkedList
{
public:
	LinkedList();
	~LinkedList();

	void AddNode(int val);
	void AddNodeToEnd(int val);
	bool AddNodeAfterValue(int newValue, int locationValue);
	LLNode * GetHeadNode();
	bool RemoveFirst();
	bool RemoveNode(int val);
	void PrintNodes();
	bool IsEmpty();

private:
	LLNode * head;
	int count;
};

